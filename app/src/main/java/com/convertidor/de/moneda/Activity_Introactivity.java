package com.convertidor.de.moneda;

import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.view.View;

import com.convertidor.moneda.R;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import agency.tango.materialintroscreen.animations.IViewTranslation;
import es.dmoral.prefs.Prefs;


public class Activity_Introactivity extends MaterialIntroActivity
{

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        enableLastSlideAlphaExitTransition(true);

        getNextButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

       //String mr = getActivity().getResources().getString(R.string.market_rates);
        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.white)
                        .buttonsColor(R.color.green)
                        .image(R.drawable.intro_first_screeen)
                        .title( getString(R.string.slide_001_title))
                        .description(getString(R.string.slide_001_descr))
                        .build());


        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.green)
                .image(R.drawable.intro_usd_source)
                .title(getString(R.string.slide_002_title))
                .description(getString(R.string.slide_002_descr))
                .build());


        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.white)
                        .buttonsColor(R.color.green)
                         .image(R.drawable.intro_second_screen)
                        .title(getString(R.string.slide_003_title))
                        .description(getString(R.string.slide_003_descr))
                        .build());


        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.green)
                .image(R.drawable.intro_tick_mark)
                .title(getString(R.string.slide_004_title))
                .description(getString(R.string.slide_004_descr))
                .build());

    }


    @Override
    public void onFinish() {
        super.onFinish();
        Prefs.with(Activity_Introactivity.this).writeInt("intro_key", 01);
       // Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
    }
}
